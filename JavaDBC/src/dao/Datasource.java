
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Datasource {
    
    private int port;
    private String hostname;
    private String database;
    private String username;
    private String password;
    
    private Connection connection;
    private Statement st;
    
    public Datasource(){
       try {
           hostname = "localhost";
           port     = 3306;
           database = "cruddb";
           username = "port";
           password = "";
           
           String url = "jdbc:mysql://"+hostname+":"+port+"/"+database;
           DriverManager.registerDriver(new com.mysql.jdbc.Driver());
           connection = (Connection) DriverManager.getConnection(url,username,password);
       }catch (SQLException ex) {
           
           System.err.println("Erro de conexão "+ex.getMessage());
       }
       
     } 
    
    public Connection getConnection(){
        return this.connection;
       } 
        
    public void ClosedDatasource(){
        try{
            connection.close();
            
        }catch(SQLException ex){
            System.err.println("Erro ao desconectar "+ex.getMessage());
        }         
    
    }
    
}
