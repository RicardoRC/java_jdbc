
package dao;

import model.Cliente;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ClienteDAO {
    private Datasource dataSource;
    
    public ClienteDAO(Datasource dataSource){
        this.dataSource = dataSource;
        
    }
    
    public ArrayList<Cliente>readAll(){
        try {
            String SQL = "SELECT * from cliente";
            PreparedStatement ps = dataSource.getConnection().prepareStatement (SQL);
            ResultSet rs = ps.executeQuery();
            
            ArrayList<Cliente> lista = new ArrayList<Cliente>();
            
            while(rs.next()){
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("id"));
                cli.steNome(rs.getString("nome"));
                cli.setEmail(rs.getString("email)"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
                
            }
              ps.close();
              return lista;
              
        }catch(SQLException ex){
            System.out.println("Erro ao recuperar "+ex.getMessage());
            
        }catch(Exception ex ) {
            System.out.println("Erro geral "+ex.getMessage());
            
        }
        
        return null;
      
    }
    
}
